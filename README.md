# CN Guidelines

## Workflow (Development Process)

* To request changes, the user SHOULD create an issue on the platform's issue
tracker, particularly in issues that require a lot of development effort or that
all beside the user's area of expertise.

* The user should not create issues for problems that can not be reproduced or
aren't properly documented.

* The release history of the project shall be the complete list of meaningful
issues solved.

* In order to solve an issue a contributor should create a fork of the project,
preferably a branch, this branch should be named according to the problem it
solves (in lower case and hyphen separated).

* Once the issue is solved on the alternate branch, the contributor must not
push directly to the master branch, instead, the contributor must first use
the project's platform to open a Merge Request (or similar).

* Each commit made must have a meaningful commit message, and must reference
what issue was resolved with it (e.g. 'Added this feature', or 'Solved this
bug'), always start with a verb.

* If some commit resolves an issue, it must be written in the message in order
to automatically close that issue.(e.g 'Added X, closes #1').

* If the platform allows merge requests (or similar) as issues, the contributor
should use this method.

* One should never merge his own merge requests (excepting urgent cases).

* Reviewers should ask for improvements to incorrect patches, and reject them
as well if the Contributor should not respond to the comments.

### Deploying

* Each project should set up continuous integration, and design tests for each
created module as long as it is possible.

* The continuous integration system should automatically deploy the software
according to the latest commit in master (as long as it passes all proposed
tests).

## Coding Guidelines

### General Rules

* No line shall exceed 80 characters.

* It is reccommended (though not required) that contributors use a pre-commit
tool to check the coding style.

* No tabs should used (use spaces instead).

### Javascript:

* Use the [semistandard](https://github.com/Flet/semistandard).
